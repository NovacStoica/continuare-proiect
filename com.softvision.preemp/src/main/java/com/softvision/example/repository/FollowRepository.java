package com.softvision.example.repository;

import com.softvision.example.model.Account;
import com.softvision.example.model.Follow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface FollowRepository extends JpaRepository<Follow, Integer> {


    @Query("select f from Follow f where f.follower = :follower and f.following = :following")
    Follow findLink(Account follower, Account following);

    List<Follow> findByFollower(Account follower);


}