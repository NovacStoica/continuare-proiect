package com.softvision.example.repository;

import com.softvision.example.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

    @Query("select a from Account a where a.userName = :userName")
     Account findByUserName(String userName);

    @Query("select a from Account a where a.accountId = :id")
     Account findById(int id);

    @Query("select a from Account a where a.userName LIKE %:name% OR a.firstName LIKE %:name% OR a.lastName LIKE %:name%")
    List<Account> findByAnyName(String name);


}