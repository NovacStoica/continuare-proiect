package com.softvision.example.repository;

import com.softvision.example.model.Account;
import com.softvision.example.model.Follow;
import com.softvision.example.model.Like;
import com.softvision.example.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface LikeRepository extends JpaRepository<Like, Integer> {



}