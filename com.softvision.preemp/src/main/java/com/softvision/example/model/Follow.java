package com.softvision.example.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="follows")

public class Follow {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int followId;

    @OneToOne
    @JoinColumn(name = "follower")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Account follower;

    @OneToOne
    @JoinColumn(name = "following")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Account following;

    private Date date = new Date();

    public Follow() {

    }


    public int getFollowId() {
        return followId;
    }

    public void setFollowId(int followId) {
        this.followId = followId;
    }

    public Account getFollower() {
        return follower;
    }

    public void setFollower(Account follower) {
        this.follower = follower;
    }

    public Account getFollowing() {
        return following;
    }

    public void setFollowing(Account following) {
        this.following = following;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    //Account getters and setters

    @JsonIgnore
    public int getFollowerId() {
        return follower.getAccountId();
    }

    @JsonIgnore
    public int getFollowingId() {
        return following.getAccountId();
    }


}