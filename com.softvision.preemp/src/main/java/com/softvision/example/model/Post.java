package com.softvision.example.model;


import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


@Entity
@Table(name="posts")
public class Post {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="postId", unique = true)
    private int postId;
    private String userName;
//    @ManyToOne
//    @JoinColumn(name = "accountId", referencedColumnName = "accountId")
//    private Account account;
    @Length(min = 3, max = 240)
    private String postContent;
    @DateTimeFormat
    private Date date = new Date();
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Like> likeList = new ArrayList<>();
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Post> repliesList = new ArrayList<>();




    public Post() {

    }

    public Post(String postContent, String userName) {
        this.userName = userName;
        this.postContent = postContent;
    }

    public List<Like> getLikeList() {
        return likeList;
    }

    public void setLikeList(List<Like> likeList) {
        this.likeList = likeList;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<Post> getRepliesList() {
        return repliesList;
    }

    public void setRepliesList(List<Post> repliesList) {
        this.repliesList = repliesList;
    }


//    public Account getAccount() {
//        return account;
//    }
//
//    public void setAccount(Account account) {
//        this.account = account;
//    }

 public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return postId == post.postId &&
                Objects.equals(postContent, post.postContent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(postContent, postId);
    }
}