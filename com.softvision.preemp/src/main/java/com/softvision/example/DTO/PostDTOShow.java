package com.softvision.example.DTO;

import com.softvision.example.model.Account;
import com.softvision.example.model.Post;
import org.hibernate.validator.constraints.Length;

import java.util.ArrayList;
import java.util.List;

public class PostDTOShow {

    private String userName;
    @Length(min = 3, max = 240)
    private String postContent;

    private List<PostDTOShow> repliesList = new ArrayList<>();

    public PostDTOShow(){

    }


    public List<PostDTOShow> getRepliesList() {
        return repliesList;
    }

    public void setRepliesList(List<PostDTOShow> repliesList) {
        this.repliesList = repliesList;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }
}
