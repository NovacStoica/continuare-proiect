package com.softvision.example.DTO;

import com.softvision.example.model.Account;
import org.hibernate.validator.constraints.Length;

public class PostDTO {

    private String userName;
    @Length(min = 3, max = 240)
    private String postContent;

    public PostDTO(){

    }



    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }
}
