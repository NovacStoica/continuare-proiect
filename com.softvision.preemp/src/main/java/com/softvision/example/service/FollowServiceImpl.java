package com.softvision.example.service;


import com.softvision.example.exceptions.CannotFollowYourself;
import com.softvision.example.exceptions.NotLoggedIn;
import com.softvision.example.model.Account;
import com.softvision.example.model.Follow;
import com.softvision.example.model.Post;
import com.softvision.example.repository.AccountRepository;
import com.softvision.example.repository.FollowRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Service
public class FollowServiceImpl {

    @Autowired
    private FollowRepository followRepository;

    @Autowired
    private AccountRepository accountRepository;




    @Autowired
    AccountServiceImpl accountServiceImpl = new AccountServiceImpl();

    @Autowired
    public FollowServiceImpl() {

    }

    /**
     * Follow an account while logged in
     * @param userName
     * @param session
     */
    public Follow follow(String userName, HttpSession session) {
        Account followed = accountRepository.findByUserName(userName);
        Account follower = accountRepository.findByUserName((String) session.getAttribute("username"));
        if(followed == follower){
            throw new CannotFollowYourself("You cannot follow yourself");
        }
        Follow f = followRepository.findLink(follower,followed);
        if (!accountServiceImpl.isLoggedIn(session)) {
            throw new NotLoggedIn("You have to be logged in to follow someone");
        } else if(f == null){
            Follow follow = new Follow();
            follow.setFollower(accountRepository.findByUserName((String) session.getAttribute("username")));
            follow.setFollowing(accountRepository.findByUserName(userName));
            return followRepository.save(follow);
         } else {
            return unfollow(follower,followed);


        }
        }

        public Follow unfollow(Account follower, Account followed){
            Follow f = followRepository.findLink(follower, followed);
            followRepository.delete(f);
            return f;
        }






}