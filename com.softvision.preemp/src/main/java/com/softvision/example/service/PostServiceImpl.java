package com.softvision.example.service;


import com.softvision.example.exceptions.NoPostFound;
import com.softvision.example.exceptions.NotLoggedIn;
import com.softvision.example.exceptions.PostDoesntExist;
import com.softvision.example.model.Account;
import com.softvision.example.model.Follow;
import com.softvision.example.model.Post;
import com.softvision.example.repository.AccountRepository;
import com.softvision.example.repository.FollowRepository;
import com.softvision.example.repository.PostRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class PostServiceImpl  {

    @Autowired
    private PostRepository postRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private FollowRepository followRepository;
    @Autowired
    private AccountServiceImpl accountService;


    ModelMapper modelMapper = new ModelMapper();


    @Autowired
    public PostServiceImpl() {
    }

    /**
     * Add a post
     * @param post
     * @param session
     * @return
     */
    public Post addPost(Post post, HttpSession session){
        String userName = (String) session.getAttribute("username");
        if(userName != null) {
            post.setUserName(userName);
            accountRepository.findByUserName(userName).getPostList().add(post);
            checkForMentions(post);
            return postRepository.save(post);
        } else {
            throw new NotLoggedIn("You are not logged in.");
        }
    }

    public List<Post> getAllPostsById(int id){
        Account a = accountRepository.findById(id);
        return a.getPostList();
    }

    /**
     * Retrieve a List of all posts
     * @return
     */
    public List<Post> retrieveAllPosts() {
        return postRepository.findAll();
    }

    /**
     * Retrieve posts made by the account that is logged in;
     * @param session
     * @return
     */
    public List<Post> retrieveMyPosts(HttpSession session){
        return accountRepository.findByUserName((String) session.getAttribute("username")).getPostList();
    }

    public List<Post> getFeed(HttpSession session){
        String userName = (String) session.getAttribute("username");
        Account a = accountRepository.findByUserName(userName);
        List<Follow> followingList = followRepository.findByFollower(a);
        List<Post> posts = new ArrayList<>();
        for(Follow f : followingList){
            posts.addAll(getAllPostsById(f.getFollowingId()));
        }
        return posts;
    }


    public void deletePost(int postId, HttpSession session){
        String postOwnerUserName = postRepository.findPostById(postId).getUserName();
        if(postOwnerUserName.equals((String) session.getAttribute("username"))) {
            accountRepository.findByUserName(postOwnerUserName).getPostList().remove(postRepository.findPostById(postId));
            postRepository.deleteById(postId);
        } else {
            throw new NoPostFound("This post doesn't belong to you so you can't delete it.");
        }
    }

    public Post repost(int postId, HttpSession session){
        if(checkIfPostExists(postId)) {
            String userName = (String) session.getAttribute("username");
            Post post = new Post();
            post.setUserName(userName);
            post.setPostContent(postRepository.findPostById(postId).getPostContent());
            accountRepository.findByUserName(userName).getPostList().add(post);
            return postRepository.save(post);
        } else {
            throw new PostDoesntExist("The parent post does not exist");
        }
    }

   public void checkForMentions(Post post){

        String content = post.getPostContent();

        for(Account a: accountService.retrieveAllAccounts()){
            if(content.contains("@" + a.getUserName())){
                a.getMentionList().add(post);
            }
        }


   }

    public List<Post> showMentionedPosts(HttpSession session) {
        String userName = (String) session.getAttribute("username");
        return accountRepository.findByUserName(userName).getMentionList();
    }

    public Post reply(int parentId, Post post, HttpSession session){
        Optional<Post> parentPost = Optional.ofNullable(postRepository.findPostById(parentId));
        if(parentPost.isPresent()) {
            postRepository.findPostById(parentId).getRepliesList().add(post);
            return addPost(post, session);
        } else {
            throw new PostDoesntExist("This post doesn't exist");
        }
    }

    public boolean checkIfPostExists(int postId){

        Optional<Post> post = Optional.ofNullable(postRepository.findPostById(postId));
        return post.isPresent();
    }











}