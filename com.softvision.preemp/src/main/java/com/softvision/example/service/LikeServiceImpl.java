package com.softvision.example.service;


import com.softvision.example.exceptions.NotLoggedIn;
import com.softvision.example.exceptions.PostDoesntExist;
import com.softvision.example.model.Like;
import com.softvision.example.model.Post;
import com.softvision.example.repository.AccountRepository;
import com.softvision.example.repository.LikeRepository;
import com.softvision.example.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

@Service
public class LikeServiceImpl {

    @Autowired
    private PostRepository postRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private LikeRepository likeRepository;
    @Autowired
    private PostServiceImpl postServiceImpl;
    @Autowired
    private AccountServiceImpl accountServiceImpl;


    /**
     * Like a post
     * @param postId
     * @param session
     */


//    public void addLike(int postId, HttpSession session) {
//        String userName = (String) session.getAttribute("username");
//
//        if (!accountServiceImpl.isLoggedIn(session)) {
//            throw new NotLoggedIn("You have to be logged in to like a post");
//        }  else if (checkIfPostLikedByPerson(postId, userName)) {
//            unlike(postId, session);
//        } else if(postServiceImpl.checkIfPostExists(postId)){
//            Like like = new Like();
//            like.setUserName(userName);
//            postRepository.findPostById(postId).getLikeList().add(like);
//            likeRepository.save(like);
//        }  else {
//            throw new PostDoesntExist("There is no post with id: " + postId);
//        }
//    }

    public void addLike(int postId, HttpSession session){
        String userName = (String) session.getAttribute("username");

        if(!accountServiceImpl.isLoggedIn(session)){
            throw new NotLoggedIn("You have to be logged in to like a post.");
        } else if(!postServiceImpl.checkIfPostExists(postId)){
            throw new PostDoesntExist("There is no post with the id: " + postId);
        } else if(checkIfPostLikedByPerson(postId,userName)){
            unlike(postId,session);
        } else {
            Like like = new Like();
            like.setUserName(userName);
            postRepository.findPostById(postId).getLikeList().add(like);
            likeRepository.save(like);
        }
    }

    //TODO: NU MERGE UNLIKE
    public void unlike(int postId, HttpSession session){
        String userName = (String) session.getAttribute("username");
        List<Like> likeList = postRepository.findPostById(postId).getLikeList();
        Like foundLike = likeList.stream().filter(l -> l.getUserName().equals(userName)).findFirst().get();
        postRepository.findPostById(postId).getLikeList().remove(foundLike);
        likeRepository.deleteById(foundLike.getLikeId());


    }

    public boolean checkIfPostLikedByPerson(int postId, String userName){
        if(postServiceImpl.checkIfPostExists(postId)) {
            Post post = postRepository.findPostById(postId);
            Optional<Like> retrieved = post.getLikeList().stream()
                    .filter(f -> f.getUserName().equals(userName)).findFirst();
            return retrieved.isPresent();
        } else {
            throw new PostDoesntExist("This post does not exist");
        }
    }



}