package com.softvision.example.service;


import com.softvision.example.DTO.AccountDTOLogged;
import com.softvision.example.exceptions.UserArleadyExists;
import com.softvision.example.exceptions.UserDoesntExist;
import com.softvision.example.model.Account;
import com.softvision.example.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.List;

@Service
public class AccountServiceImpl {

    @Autowired
    private AccountRepository accountRepository;




    AccountDTOLogged accountDTOLogged = new AccountDTOLogged();

    @Autowired
    public AccountServiceImpl() {

    }


    /**
     * Register an account
     * @param account
     * @return
     */
    public Account save(Account account){

        if(accountRepository.findByUserName(account.getUserName()) == null) {
            return accountRepository.save(account);

        } else {
            throw new UserArleadyExists("This user is arleady registered") ;
        }


    }

    /**
     * Retrieve all accounts
     * @return
     */
    public List<Account> retrieveAllAccounts(){
        return accountRepository.findAll();
    }

    /**
     * Search by name
     * @param name
     * @return
     */
    public List<Account> search(String name){
       return accountRepository.findByAnyName(name);
    }

    public Account searchById(int accountId) throws UserDoesntExist {
        if(accountRepository.findById(accountId) == null) {
            throw new UserDoesntExist("There is no account with this id.");
        } else {
            return accountRepository.findById(accountId);
        }
    }
    /**
     * Login
     * @param username
     * @param password
     * @param session
     * @return
     */
    public AccountDTOLogged login(String username, String password, HttpSession session) {
        if(accountRepository.findByUserName(username) == null) {
            throw new UserDoesntExist("This username doesn't exist.");
        } else {

            Account account = accountRepository.findByUserName(username);
            if (password.equals(account.getPassword())) {
                Calendar now = Calendar.getInstance();



                session.setAttribute("username", username);
                String str = session.getAttribute("username").toString();
                synchronized (AccountDTOLogged.class) {
                    this.accountDTOLogged.setUserName(str);
                accountDTOLogged.setLogged(true);
                if(now.get(Calendar.MINUTE)%2==0){
                    try {
                        Thread.sleep(60000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                    System.out.println("User" + accountDTOLogged.getUserName() + " logged in.");
                    return accountDTOLogged;
                }
            }
        }
        return null;
    }

    /**
     * Logout
     * @param session
     */
    public void logout(HttpSession session){
        session.invalidate();
    }

    /**
     * Check if user is logged in;
     * @param session
     * @return
     */
    public boolean isLoggedIn(HttpSession session){

        return session.getAttribute("username") == null;
    }

    public void deleteAccount(HttpSession session){
        int accountId = accountRepository.findByUserName((String) session.getAttribute("username")).getAccountId();
        accountRepository.deleteById(accountId);
    }





}