package com.softvision.example.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class CannotFollowYourself extends RuntimeException {

    public CannotFollowYourself(String message) {
        super(message);
    }
}



