package com.softvision.example.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class UserArleadyExists extends RuntimeException {

    public UserArleadyExists(String message) {
        super(message);
    }
}
