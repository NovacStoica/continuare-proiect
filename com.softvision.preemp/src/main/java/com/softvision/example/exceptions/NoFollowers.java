package com.softvision.example.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoFollowers extends RuntimeException {

    public NoFollowers(String message) {
        super(message);
    }
}


