package com.softvision.example.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class NotLoggedIn extends RuntimeException {

    public NotLoggedIn(String message) {
        super(message);
    }
}


