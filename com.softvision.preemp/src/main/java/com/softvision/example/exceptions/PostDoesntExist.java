package com.softvision.example.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PostDoesntExist extends RuntimeException {

    public PostDoesntExist(String message) {
        super(message);
    }
}
