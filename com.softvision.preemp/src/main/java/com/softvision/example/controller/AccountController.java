package com.softvision.example.controller;

import com.softvision.example.DTO.AccountCreateDTO;
import com.softvision.example.DTO.AccountDTO;
import com.softvision.example.DTO.AccountDTOLogged;
import com.softvision.example.mapper.ObjectMapper;
import com.softvision.example.model.Account;
import com.softvision.example.service.AccountServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
@Component
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountServiceImpl accountServiceImpl;

    private ModelMapper modelMapper = new ModelMapper();


    /**
     * Returns all accounts created
     *
     * @return
     */
    @RequestMapping(
            value = "/",
            method = RequestMethod.GET,
            produces = "application/json")
    List<AccountDTO> retrieveAccounts() {
        return ObjectMapper.mapAll(this.accountServiceImpl.retrieveAllAccounts(), AccountDTO.class);
    }
    // Registers a person
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(
            value = "/register",
            method = RequestMethod.POST,
            produces = "application/json", consumes = "application/json")
    Account saveAccount(@RequestBody @Valid AccountCreateDTO accountDTO) {
        return this.accountServiceImpl.save(modelMapper.map(accountDTO, Account.class));
    }


    //Searches by any name
    @RequestMapping(
            value = "/search/{name}",
            method = RequestMethod.GET,
            produces = "application/json")
    List<AccountDTO> searchByName(@PathVariable String name) {
        return ObjectMapper.mapAll(this.accountServiceImpl.search(name), AccountDTO.class);
    }

    @RequestMapping(
            value = "/search/id/{accountId}",
            method = RequestMethod.GET,
            produces = "application/json")
    AccountDTO searchById(@PathVariable int accountId) {
        return modelMapper.map(accountServiceImpl.searchById(accountId), AccountDTO.class);
    }
    //Login
    @GetMapping(value="/login")
    public ResponseEntity<AccountDTOLogged> login(@RequestHeader(value = "username") String username,
                                                  @RequestHeader(value = "password") String password,
                                                  HttpSession session
    ) {
        this.accountServiceImpl.login(username,password,session);
            return new ResponseEntity<>(HttpStatus.OK);


    }


    //Logout
    @GetMapping(value="/logout")
    public void logout(HttpSession session){
        this.accountServiceImpl.logout(session);
    }

    @RequestMapping(
            value = "/delete",
            method = RequestMethod.POST,
            produces = "application/json")
    void deleteAccount( HttpSession session ) {
        accountServiceImpl.deleteAccount(session);
    }






    }
