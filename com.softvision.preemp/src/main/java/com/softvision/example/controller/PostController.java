package com.softvision.example.controller;


import com.softvision.example.DTO.PostDTO;
import com.softvision.example.DTO.PostDTOShow;
import com.softvision.example.mapper.ObjectMapper;
import com.softvision.example.model.Post;
import com.softvision.example.service.AccountServiceImpl;
import com.softvision.example.service.PostServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/post")
public class PostController {

    @Autowired
    private PostServiceImpl postServiceImpl;

    @Autowired
    private AccountServiceImpl accountServiceImpl;

    private ModelMapper modelMapper = new ModelMapper();

    /**
     * Retrieve all posts ever made;
     *
     * @return
     */
    @RequestMapping(
            value = "/",
            method = RequestMethod.POST,
            produces = "application/json")
    Post addPost(@RequestBody  PostDTO postDTO, HttpSession session) {
        Post post = modelMapper.map(postDTO, Post.class);
        return postServiceImpl.addPost(post, session);
    }


    @RequestMapping(
            value = "/",
            method = RequestMethod.GET,
            produces = "application/json")
    List<PostDTO> retrieveAllPosts() {
        List<PostDTO> listOfPostDTO = ObjectMapper.mapAll(this.postServiceImpl.retrieveAllPosts(), PostDTO.class);
        return listOfPostDTO;
    }

    @RequestMapping(
            value="/myposts",
            method = RequestMethod.GET,
            produces = "application/json")
    List<PostDTOShow> retrieveMyPosts(HttpSession session){
        List<PostDTOShow> listOfPostDTO = ObjectMapper.mapAll(this.postServiceImpl.retrieveMyPosts(session), PostDTOShow.class);
        return listOfPostDTO;

    }

    @RequestMapping(
            value="/feed",
            method = RequestMethod.GET,
            produces = "application/json")
    List<PostDTOShow> feed(HttpSession session){
        List<PostDTOShow> listOfPostDTO = ObjectMapper.mapAll(this.postServiceImpl.getFeed(session), PostDTOShow.class);
        return listOfPostDTO;

    }

    @RequestMapping(
            value="/delete/{postId}",
            method = RequestMethod.POST,
            produces = "application/json")
    void deletePost(@PathVariable int postId, HttpSession session){

            postServiceImpl.deletePost(postId, session);

    }

    @RequestMapping(
            value = "/repost/{postId}",
            method = RequestMethod.POST,
            produces = "application/json")
    Post repost(@PathVariable int postId, HttpSession session) {
        return postServiceImpl.repost(postId,session);
    }
    @RequestMapping(
            value="/mentioned",
            method = RequestMethod.GET,
            produces = "application/json")
    List<PostDTO> showMentionedPosts(HttpSession session){
        List<PostDTO> listOfPostDTO = ObjectMapper.mapAll(this.postServiceImpl.showMentionedPosts(session), PostDTO.class);
        return listOfPostDTO;

    }
    @RequestMapping(
            value="/reply/{parentId}",
            method = RequestMethod.POST,
            produces = "application/json")
        Post Reply(@PathVariable int parentId, @RequestBody PostDTO postDTO,  HttpSession session){
        Post post = modelMapper.map(postDTO, Post.class);
        return postServiceImpl.reply(parentId, post, session);

    }


}