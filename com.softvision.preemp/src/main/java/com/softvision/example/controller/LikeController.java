package com.softvision.example.controller;

import com.softvision.example.service.LikeServiceImpl;
import com.softvision.example.service.PostServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/like")
public class LikeController {

    @Autowired
    private LikeServiceImpl likeServiceImpl;
    @Autowired
    private PostServiceImpl postServiceImpl;




    @RequestMapping(
            value = "/{postId}",
            method = RequestMethod.POST,
            produces = "application/json")
    void addLike(@PathVariable int postId, HttpSession session) {
       likeServiceImpl.addLike(postId,session);
    }




}