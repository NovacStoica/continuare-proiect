package com.softvision.example.controller;


import com.softvision.example.model.Follow;
import com.softvision.example.service.FollowServiceImpl;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/follow")
public class FollowController {

    @Autowired
    private FollowServiceImpl followServiceImpl;


    @RequestMapping(
            value = "/{userName}",
            method = RequestMethod.POST,
            produces = "application/json")
    public void follow(@PathVariable @Length(min = 3, max = 15) String userName, HttpSession session) {
        this.followServiceImpl.follow(userName, session);

    }

//    @RequestMapping(
//            value = "/get",
//            method = RequestMethod.GET,
//            produces = "application/json")
//    public List<Follow> getFollowing( HttpSession session) {
//        return followServiceImpl.getFollowing(session);
//
//    }


}