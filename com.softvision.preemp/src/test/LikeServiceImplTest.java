import com.softvision.example.exceptions.NotLoggedIn;
import com.softvision.example.model.Like;
import com.softvision.example.model.Post;
import com.softvision.example.repository.AccountRepository;
import com.softvision.example.repository.LikeRepository;
import com.softvision.example.repository.PostRepository;
import com.softvision.example.service.AccountServiceImpl;
import com.softvision.example.service.LikeServiceImpl;
import com.softvision.example.service.PostServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class LikeServiceImplTest {

    @Mock
    private PostRepository postRepository;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private LikeRepository likeRepository;
    @Mock
    private PostServiceImpl postService;
    @Mock
    private AccountServiceImpl accountService;

    @InjectMocks
    LikeServiceImpl likeService = new LikeServiceImpl();

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = NotLoggedIn.class)
    public void addLike_whenUserIsNotLoggedIn_returnException(){
        HttpSession session = mock(HttpSession.class);
        int postId = 1 ;

        when(!accountService.isLoggedIn(session)).thenReturn(false);

        likeService.addLike(postId, session);

    }

    @Test
    public void addLike_whenUserIsLoggedInAndPostIsntLiked_likePost(){
        HttpSession session = mock(HttpSession.class);
        Like like = new Like();
        Post post = new Post();
        int postId = 1;
        post.setPostId(postId);
        List<Like> likeList = new ArrayList<>();
        post.setLikeList(likeList);
        LikeServiceImpl likeServ = new LikeServiceImpl();
        LikeServiceImpl likeSpy = spy(likeServ);
        when(session.getAttribute("username")).thenReturn("vali");
        when(!accountService.isLoggedIn(session)).thenReturn(true);
        when(postRepository.findPostById(postId)).thenReturn(post);
        when(!postService.checkIfPostExists(postId)).thenReturn(true);
        doReturn(false).when(likeSpy).checkIfPostLikedByPerson(postId,"vali");

        likeService.addLike(1,session);

        assertFalse(post.getLikeList().isEmpty());
    }
//    @Test
//    public void addLike_whenUserIsLoggedInAndPostIsLiked_unlikePost(){
//        HttpSession session = mock(HttpSession.class);
//        int postId =  1;
//        String userName = "vali";
//        when(!accountService.isLoggedIn(session)).thenReturn(true);
//        when(!postService.checkIfPostExists(postId)).thenReturn(true);
//        LikeServiceImpl likeServ = new LikeServiceImpl();
//        LikeServiceImpl likeSpy = spy(likeServ);
//        doReturn(true).when(likeSpy).checkIfPostLikedByPerson(postId,userName);
//
//    }


    @Test
    public void checkIfPostLikedByPerson_whenItIsLiked_returnTrue() {
        int postId = 1 ;
        String userName = "vali";
        Post post = new Post();
        Like like = new Like();
        like.setUserName("vali");
        post.getLikeList().add(like);
        when(postRepository.findPostById(postId)).thenReturn(post);
        when(postService.checkIfPostExists(postId)).thenReturn(true);

        assertTrue(likeService.checkIfPostLikedByPerson(postId,userName));
    }

    @Test
    public void checkIfPostLikedByPerson_whenItIsNotLiked_returnFalse() {
        int postId = 1 ;
        String userName = "vali";
        Post post = new Post();
        Like like = new Like();
        like.setUserName("vali");
        when(postService.checkIfPostExists(postId)).thenReturn(true);
        when(postRepository.findPostById(postId)).thenReturn(post);

        assertFalse(likeService.checkIfPostLikedByPerson(postId,userName));
    }


}
