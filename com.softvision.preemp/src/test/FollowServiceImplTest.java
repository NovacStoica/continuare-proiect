import com.softvision.example.exceptions.CannotFollowYourself;
import com.softvision.example.exceptions.NotLoggedIn;
import com.softvision.example.model.Account;
import com.softvision.example.model.Follow;
import com.softvision.example.repository.AccountRepository;
import com.softvision.example.repository.FollowRepository;
import com.softvision.example.service.AccountServiceImpl;
import com.softvision.example.service.FollowServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.http.HttpSession;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;

public class FollowServiceImplTest {

    @Mock
    private FollowRepository followRepository;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private AccountServiceImpl accountService;

    @InjectMocks
    private FollowServiceImpl followService = new FollowServiceImpl();


    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }


    @Test(expected = CannotFollowYourself.class)
    public void follow_whenFollowingYourself_returnException() {
        HttpSession session = mock(HttpSession.class);
        String userName = "vali";
        Account account = new Account();
        when(session.getAttribute("username")).thenReturn("vali");
        when(accountRepository.findByUserName("vali")).thenReturn(account);
    }
    @Test(expected = NotLoggedIn.class)
    public void follow_whenUserIsNotLoggedIn_returnException(){
        String userName = "vali";
        HttpSession session = mock(HttpSession.class);
        Account followed = new Account();
        Account follower = new Account();
        Follow follow = new Follow();
        when(accountRepository.findByUserName(userName)).thenReturn(followed);
        when(session.getAttribute("username")).thenReturn("valeriu");
        when(accountRepository.findByUserName("valeriu")).thenReturn(follower);
        when(followRepository.findLink(follower,followed)).thenReturn(follow);
        when(!accountService.isLoggedIn(session)).thenReturn(false);

        followService.follow(userName,session);
    }

    @Test
    public void follow_whenUserIsLoggedInAndLinkDoesntExist_follow(){
        String userName = "vali";
        HttpSession session = mock(HttpSession.class);
        Account followed = new Account();
        followed.setAccountId(1);
        Account follower = new Account();
        follower.setAccountId(2);
        when(accountRepository.findByUserName(userName)).thenReturn(followed);
        when(session.getAttribute("username")).thenReturn("valeriu");
        when(accountRepository.findByUserName("valeriu")).thenReturn(follower);
        when(followRepository.findLink(follower,followed)).thenReturn(null);
        when(!accountService.isLoggedIn(session)).thenReturn(true);

        followService.follow(userName,session);
        assertTrue(!followRepository.findAll().isEmpty());

    }
}
