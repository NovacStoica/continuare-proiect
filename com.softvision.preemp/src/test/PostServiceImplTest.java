
import com.softvision.example.exceptions.NotLoggedIn;
import com.softvision.example.exceptions.PostDoesntExist;
import com.softvision.example.model.Account;
import com.softvision.example.model.Follow;
import com.softvision.example.model.Post;
import com.softvision.example.repository.AccountRepository;
import com.softvision.example.repository.FollowRepository;
import com.softvision.example.repository.PostRepository;
import com.softvision.example.service.AccountServiceImpl;
import com.softvision.example.service.PostServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


public class PostServiceImplTest {

    @Mock
    PostRepository postRepository;
    @Mock
    AccountRepository accountRepository;
    @Mock
    FollowRepository followRepository;
    @Mock
    AccountServiceImpl accountService;

    @InjectMocks
    PostServiceImpl postService = new PostServiceImpl();


    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = NotLoggedIn.class)
    public void addPost_whenIsNotLoggedIn_returnException(){
        Post post = new Post();
        HttpSession session = mock(HttpSession.class);
        session.setAttribute("username",null);

        postService.addPost(post,session);

    }
    @Test
    public void addPost_whenIsLoggedIn_returnPost(){
        HttpSession session = mock(HttpSession.class);
        Post post = new Post();
        Account account = new Account();
        account.setUserName("vali");
        List<Account> accountList = new ArrayList<>();
        when(session.getAttribute("username")).thenReturn("vali");
        when(accountRepository.findByUserName("vali")).thenReturn(account);
        when(accountService.retrieveAllAccounts()).thenReturn(accountList);
        postService.addPost(post,session);

        assertTrue(account.getPostList().contains(post));


    }


    @Test
    public void getAllPostsById_returnPostList() {
        Account account = new Account();
        List<Post> postList = new ArrayList<>();
        Post post = new Post();
        postList.add(post);
        account.setPostList(postList);

        when(accountRepository.findById(1)).thenReturn(account);

        List<Post> retrievedList = postService.getAllPostsById(1);

        assertTrue(retrievedList.contains(post));

    }

    @Test
    public void retrieveAllPosts_returnPosts(){
        List<Post> postList = new ArrayList<>();
        when(postRepository.findAll()).thenReturn(postList);

        List<Post> retrievedList = postService.retrieveAllPosts();

        assertTrue(retrievedList.equals(postList));

    }

    @Test
    public void retrieveMyPosts_returnMyPosts(){
        HttpSession session = mock(HttpSession.class);
        Account account = new Account();
        List<Post> myPostList = new ArrayList<>();
        Post post = new Post();
        myPostList.add(post);
        account.setPostList(myPostList);
        session.setAttribute("username","vali");

        when(accountRepository.findByUserName("vali")).thenReturn(account);
        when(session.getAttribute("username")).thenReturn("vali");
        List<Post> retrievedList = postService.retrieveMyPosts(session);

        assertTrue(retrievedList.equals(myPostList));
    }
    //NU MERGE
    @Test
    public void getFeed_returnFeed(){
       HttpSession session = mock(HttpSession.class);
       Account account = new Account();
       Account account2 = mock(Account.class);
       Follow follow = mock(Follow.class);
       List<Follow> followingList = new ArrayList<>();
       followingList.add(follow);
       List<Post> postList = new ArrayList<>();

       when(session.getAttribute("username")).thenReturn("vali");
       when(accountRepository.findByUserName("vali")).thenReturn(account);
       when(followRepository.findByFollower(account)).thenReturn(followingList);
       when(accountRepository.findById(1)).thenReturn(account2);
       when(follow.getFollowingId()).thenReturn(1);
       when(postService.getAllPostsById(1)).thenReturn(postList);

       List<Post> responseList = postService.getFeed(session);

       assertTrue(responseList.equals(postList));

    }

    @Test
    public void repost_returnPost(){
        HttpSession session = mock(HttpSession.class);
        Post post = new Post();
        post.setPostId(1);
        post.setPostContent("dada");
        Post originalPost = mock(Post.class);
        Account account = mock(Account.class);
        List<Post> postList = new ArrayList<>();
        List<Follow> followingList = new ArrayList<>();
        Follow follow = mock(Follow.class);

        when(session.getAttribute("username")).thenReturn("vali");
        when(originalPost.getPostContent()).thenReturn("dada");
        when(postRepository.findPostById(anyInt())).thenReturn(originalPost);
        when(account.getPostList()).thenReturn(postList);
        when(accountRepository.findByUserName("vali")).thenReturn(account);
        when(followRepository.findByFollower(account)).thenReturn(followingList);
        when(postRepository.save(any(Post.class))).thenReturn(post);

        Post responsePost = postService.repost(1, session);

        assertTrue(responsePost.getPostContent().equals("dada"));
    }

    @Test
    public void checkForMentions_whenItIsMentioned_addToMentionList() {
        Post post = new Post();
        post.setPostContent("da @vali da");
        List<Account> accountList = new ArrayList<>();
        Account account = new Account();
        account.setUserName("vali");
        accountList.add(account);

        when(accountService.retrieveAllAccounts()).thenReturn(accountList);
        postService.checkForMentions(post);

        assertTrue(account.getMentionList().contains(post));

    }
    @Test
    public void checkForMentions_whenItIsNotMentioned_doNothing() {
        Post post = new Post();
        post.setPostContent("da da");
        List<Account> accountList = new ArrayList<>();
        Account account = new Account();
        account.setUserName("vali");
        accountList.add(account);

        when(accountService.retrieveAllAccounts()).thenReturn(accountList);
        postService.checkForMentions(post);

        assertFalse(account.getMentionList().contains(post));

    }

    @Test
    public void showMentionedPosts() {
        HttpSession session = mock(HttpSession.class);
        Account account = new Account();
        Post post = new Post();
        account.setUserName("vali");
        account.getMentionList().add(post);

        when(session.getAttribute("username")).thenReturn("vali");
        when(accountRepository.findByUserName("vali")).thenReturn(account);

        List<Post> mentionList = postService.showMentionedPosts(session);

        assertTrue(mentionList.contains(post));

    }

    @Test
    public void reply_whenParentPostExists_addPost(){
        HttpSession session = mock(HttpSession.class);
        Post post = new Post();
        int parentId = 1;

        when(session.getAttribute("username")).thenReturn("vali");
        when(postRepository.findPostById(parentId)).thenReturn(post);
        Account account = new Account();
        account.setUserName("vali");
        List<Account> accountList = new ArrayList<>();
        when(session.getAttribute("username")).thenReturn("vali");
        when(accountRepository.findByUserName("vali")).thenReturn(account);
        when(accountService.retrieveAllAccounts()).thenReturn(accountList);
        when(postService.addPost(post,session)).thenReturn(post);

        Post returnedPost = postService.reply(parentId, post , session);

        assertTrue(returnedPost == post);

    }

    @Test(expected = PostDoesntExist.class)
    public void reply_whenParentPostDoesntExist_returnException(){
        HttpSession session = mock(HttpSession.class);
        Post post = new Post();
        int parentId = 1;

        when(postRepository.findPostById(parentId)).thenReturn(null);

        postService.reply(parentId,post,session);

    }


}
