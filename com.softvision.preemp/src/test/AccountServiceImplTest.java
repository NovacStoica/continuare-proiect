

import com.softvision.example.DTO.AccountDTOLogged;
import com.softvision.example.exceptions.UserArleadyExists;
import com.softvision.example.exceptions.UserDoesntExist;
import com.softvision.example.model.Account;
import com.softvision.example.repository.AccountRepository;
import com.softvision.example.service.AccountServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class AccountServiceImplTest {

    @Mock
    AccountRepository accountRepository;
    @InjectMocks
    AccountServiceImpl accountService = new AccountServiceImpl();



    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }


    @Test(expected = UserArleadyExists.class)
    public void save_whenUsernameArleadyExists_returnException(){
        Account account = new Account();
        account.setUserName("vali");

        when(accountRepository.findByUserName("vali")).thenReturn(account);

        accountService.save(account);



    }

    @Test
    public void save_whenUsernameDoesntExist_registerAccount(){
        Account account = new Account();
        account.setUserName("vali");
        List<Account> a = new ArrayList<>();

        when(accountRepository.findByUserName(anyString())).thenReturn(null);
        when(accountRepository.save(any(Account.class))).thenReturn(account);

        Account created = accountService.save(account);
        assertTrue(created.getUserName() == "vali");


    }

    @Test
    public void retrieveAllAccounts_returnAccounts(){
       List<Account> accountList = new ArrayList<>();
        when(accountRepository.findAll()).thenReturn(accountList);

        List<Account> retrievedList = accountService.retrieveAllAccounts();

        assertTrue(retrievedList == accountList);

    }
    @Test
    public void search_returnAccounts(){
        Account account = new Account();
        List<Account> searchedList = new ArrayList<>();
        searchedList.add(account);

        when(accountRepository.findByAnyName("nelu")).thenReturn(searchedList);

        List<Account> retrievedList = accountService.search("nelu");

        assertTrue(searchedList == retrievedList);

    }

    @Test
    public void searchById_whenUserDoesntExist_returnException(){
        Account account = new Account();
        account.setUserName("vali");

        when(accountRepository.findById(1)).thenReturn(null);

        try {
            accountService.searchById(1);
        } catch(UserDoesntExist e){
            assertTrue(e.getMessage().equals("There is no account with this id."));
        }


    }

    @Test
    public void searchById_whenUserExists_returnAccount(){
        Account account = new Account();
        account.setAccountId(1);

        when(accountRepository.findById(1)).thenReturn(account);

        Account retrievedAccount = accountService.searchById(1);

        assertTrue(account == retrievedAccount);

    }

    @Test(expected = UserDoesntExist.class)
    public void login_whenUserDoesntExistAndPasswordIsCorrect_returnException(){
        Account account = new Account();
        account.setUserName("vali");
        account.setPassword("dada");
        HttpSession session = mock(HttpSession.class);
        when(accountRepository.findByUserName("vali")).thenReturn(null);

        accountService.login("vali","dada", session);
    }

    @Test
    public void login_whenUserExistsAndPasswordIsCorrect_login() {
        Account account = new Account();
        account.setUserName("vali");
        account.setPassword("dada");
        HttpSession session = mock(HttpSession.class);
        when(accountRepository.findByUserName("vali")).thenReturn(account);
        when(session.getAttribute("username")).thenReturn("vali");
        AccountDTOLogged accountDtoLogged = accountService.login("vali","dada", session);

        assertTrue(accountDtoLogged.isLogged());


    }
    @Test
    public void login_whenUserExistsButPasswordIsWrong_returnNull(){
        Account account = new Account();
        account.setUserName("vali");
        account.setPassword("dada");
        HttpSession session = mock(HttpSession.class);
        when(accountRepository.findByUserName("vali")).thenReturn(account);
        when(session.getAttribute("username")).thenReturn("vali");
        AccountDTOLogged accountDtoLogged = accountService.login("vali","nunu", session);

        assertNull(accountDtoLogged);
    }

    @Test
    public void logout(){
        HttpSession session = mock(HttpSession.class);
        session.setAttribute("username","vali");

        accountService.logout(session);

        assertNull(session.getAttribute("username"));
    }

    @Test
    public void isLoggedIn_whenIsLoggedIn_returnTrue(){
        HttpSession session = mock(HttpSession.class);

        when(session.getAttribute("username")).thenReturn(anyString());

        assertTrue(accountService.isLoggedIn(session));
    }
    @Test
    public void isLoggedIn_whenIsNotLoggedIn_returnFalse(){
        HttpSession session = mock(HttpSession.class);

        when(session.getAttribute("username")).thenReturn(null);

        assertFalse(accountService.isLoggedIn(session));
    }






    }
