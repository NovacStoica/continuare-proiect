import com.softvision.example.ExampleApp;
import com.softvision.example.controller.AccountController;
import com.softvision.example.model.Account;
import com.softvision.example.service.AccountServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ExampleApp.class)
@AutoConfigureMockMvc
public class AccountControllerTest {


    @Mock
    private AccountServiceImpl accountService;
    @InjectMocks
    private AccountController accountController = new AccountController();

    private MockMvc mockMvc;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(accountController).build();
    }
//    @Autowired
//    private AccountController accountController;
//    @MockBean
//    private AccountServiceImpl accountService;





    @Test
    public void retrieveAccounts() throws Exception {
        List<Account> accountList = new ArrayList<>();
        Account account = new Account();
        account.setUserName("test1");
        account.setFirstName("test2");
        account.setLastName("test3");

        accountList.add(account);
        when(accountService.retrieveAllAccounts()).thenReturn(accountList);
        this.mockMvc.perform(get("/account/")).andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].userName").value("test1"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].firstName").value("test2"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$[0].lastName").value("test3"));

    }

    @Test
    public void saveAccount() throws Exception {
        Account account = new Account();
        account.setPassword("test1234");
        account.setEmail("tes1t@gmail.com");
        account.setUserName("Test1");
        account.setFirstName("Test1");
        account.setLastName("Test2");
        String json = "{\n" +
                "\t\t\"userName\": \"Test1\",\n" +
                "        \"firstName\": \"Test1\",\n" +
                "        \"lastName\": \"Test2\",\n" +
                "        \"email\": \"tes1t@gmail.com\",\n" +
                "        \"password\": \"test1234\"\n" +
                "    }";
        when(accountService.save(account)).thenReturn(account);
        this.mockMvc.perform(post("/account/register/")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .content(json))
                    .andExpect(status().isCreated());

//                .andExpect(status().isCreated())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(MockMvcResultMatchers.jsonPath("$[0].userName").value("test1"))
//                .andExpect(MockMvcResultMatchers.jsonPath("$[0].firstName").value("test2"))
//                .andExpect(MockMvcResultMatchers.jsonPath("$[0].lastName").value("test3"));

    }



}
